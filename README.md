# Django Docker Sample

Run the following command:

```
docker-compose up -d
```

To regenerate staticfiles:
```
docker-compose exec web python manage.py collectstatic --no-input
```